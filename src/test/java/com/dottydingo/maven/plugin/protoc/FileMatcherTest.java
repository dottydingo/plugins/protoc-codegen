package com.dottydingo.maven.plugin.protoc;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileMatcherTest
{
    @Test
    void include_all() {
        FileMatcher fileMatcher  = new FileMatcher(new String[]{".*"},new String[]{});
        assertTrue(fileMatcher.match("test"));
        assertTrue(fileMatcher.match("test/foo/bar"));
        assertTrue(fileMatcher.match("test/foo/bar.proto"));
    }

    @Test
    void skip_domain() {
        FileMatcher fileMatcher  = new FileMatcher(new String[]{".*"},new String[]{".*google"});
        assertTrue(fileMatcher.match("test"));
        assertTrue(fileMatcher.match("test/foo/bar"));
        assertFalse(fileMatcher.match("google/foo/bar.proto"));
    }

    @Test
    void explicit_include() {
        FileMatcher fileMatcher  = new FileMatcher(new String[]{"test/foo/bar"},new String[]{});
        assertFalse(fileMatcher.match("test"));
        assertTrue(fileMatcher.match("test/foo/bar"));
        assertFalse(fileMatcher.match("google/foo/bar.proto"));
    }
}