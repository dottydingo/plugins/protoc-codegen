package com.dottydingo.maven.plugin.protoc;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.resolver.ResolutionErrorHandler;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.sonatype.plexus.build.incremental.BuildContext;

import java.io.File;
import java.io.IOException;
import java.util.List;

public abstract class AbstractCodegenMojo extends AbstractMojo
{
    /**
     * A directory where temporary files will be generated.
     */
    @Parameter(
            required = true,
            defaultValue = "${project.build.directory}/protoc-plugins"
    )
    private File workingDirectory;

    /**
     * Protobuf compiler artifact specification, in {@code groupId:artifactId:version[:type[:classifier]]} format.
     */
    @Parameter(
            required = true,
            property = "protocArtifact"
    )
    private String protocArtifact;

    /**
     * If set to {@code true}, then the specified protobuf source files from this project will be attached
     * as resources to the build, for subsequent inclusion into the final artifact.
     * This is the default behaviour, as it allows downstream projects to import protobuf definitions
     * from the upstream projects, and those imports are automatically resolved at build time.
     *
     * <p>If distribution of {@code .proto} source files is undesirable for security reasons
     * or because of other considerations, then this parameter should be set to {@code false}.</p>
     */
    @Parameter(
            required = false,
            defaultValue = "true"
    )
    private boolean attachProtoSources;

    /**
     * The resource location the proto sources will be if (@code attachProtoSources} set to {@code true}.
     * The default value is {@code proto}.
     */
    @Parameter(
            required = false,
            defaultValue = "proto"
    )
    protected String protoResourceLocation;

    /**
     * If set to {@code true}, all command line arguments to protoc will be written to a file,
     * and only a path to that file will be passed to protoc on the command line.
     * This helps prevent <i>Command line is too long</i> errors when the number of {@code .proto} files is large.
     *
     * <p><b>NOTE:</b> This is only supported for protoc 3.5.0 and higher.</p>
     */
    @Parameter(
            required = false,
            defaultValue = "false"
    )
    private boolean useArgumentFile;

    /**
     * This is the directory where the generated code will be placed.
     * If this parameter is unspecified, then the default location is constructed as follows:<br>
     * <code>${project.build.directory}/generated-sources/protobuf/&lt;pluginId&gt;</code>
     */
    @Parameter(
            required = true,
            defaultValue = "${project.build.directory}/generated-sources/protoc-gen"
    )
    private File outputDirectory;

    @Parameter(required = true)
    private List<OutputTarget> outputTargets;

    /**
     * The current Maven project.
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    protected MavenProject project;

    /**
     * The current Maven Session Object.
     */
    @Parameter(defaultValue = "${session}", readonly = true)
    private MavenSession session;

    /**
     * Build context that tracks changes to the source and target files.
     */
    @Component
    private BuildContext buildContext;

    @Component
    private RepositorySystem repositorySystem;

    @Component
    private ResolutionErrorHandler resolutionErrorHandler;


    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        if(outputTargets.isEmpty()) {
            throw new InitializationException("No output targets specified");
        }

        ArtifactFileResolver artifactFileResolver = new ArtifactFileResolver(getLog(),
                session.getLocalRepository(),
                project.getRemoteArtifactRepositories(),
                session,
                repositorySystem,
                resolutionErrorHandler,
                workingDirectory,
                project.getArtifact());

        JavaPluginBuilder javaPluginBuilder = new JavaPluginBuilder(artifactFileResolver,
                getLog(),workingDirectory);

        try {
            FileUtils.forceMkdir(outputDirectory);
        } catch (IOException e) {
            throw new InitializationException("Error creating output directory " + outputDirectory.getAbsolutePath());
        }

        try {
            FileUtils.forceMkdir(workingDirectory);
        } catch (IOException e) {
            throw new InitializationException("Error creating working directory " + workingDirectory.getAbsolutePath());
        }

        Artifact artifact = artifactFileResolver.createDependencyArtifact(protocArtifact);
        if(artifact == null) {
            throw new InitializationException(String.format("Could not create and artifact for '%s'",protocArtifact));
        }
        getLog().info("Found " + artifact.toString());
        File protocFile = artifactFileResolver.resolveBinaryArtifact(artifact);

        List<String> protoFiles = getProtoFiles();
        getLog().info("files: " + protoFiles);
        if(!protoFiles.isEmpty()) {

            processAdditionalDependencies(artifactFileResolver);

            ProtocOutputTargetBuilder outputTargetBuilder = new ProtocOutputTargetBuilder(
                    artifactFileResolver,javaPluginBuilder, outputDirectory);

            Protoc.Builder builder = new Protoc.Builder(protocFile.getAbsolutePath());
            builder.setWorkingDirectory(workingDirectory)
                    .addProtoFiles(protoFiles);

            builder.addOutputTargets(outputTargetBuilder.buildTargets(outputTargets));

            addBuilderParameters(builder);

            Protoc protoc = builder.build();
            try {
                int result = protoc.execute(getLog());

                if (StringUtils.isNotBlank(protoc.getOutput())) {
                    getLog().info("PROTOC: " + protoc.getOutput());
                }
                if (result != 0) {
                    getLog().error("PROTOC FAILED: " + protoc.getError());
                    throw new MojoFailureException(
                            "protoc did not exit cleanly. Review output for more information.");
                } else if (StringUtils.isNotBlank(protoc.getError())) {
                    getLog().warn("PROTOC: " + protoc.getError());
                }
            } catch (CommandLineException e) {
                throw new MojoExecutionException("Protoc failed.",e);
            }

            if(attachProtoSources) {
                attachFiles();
            }

            project.addCompileSourceRoot(outputDirectory.getAbsolutePath());
            buildContext.refresh(outputDirectory);
        } else {
            getLog().warn("No proto files found");
        }
    }

    protected void processAdditionalDependencies(ArtifactFileResolver artifactFileResolver) {}

    protected abstract void addBuilderParameters(Protoc.Builder builder);

    protected abstract List<String> getProtoFiles();

    protected void attachFiles() {}
}
