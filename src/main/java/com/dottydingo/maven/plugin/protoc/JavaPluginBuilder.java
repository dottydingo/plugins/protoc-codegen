package com.dottydingo.maven.plugin.protoc;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.Os;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

public class JavaPluginBuilder
{
    private final ArtifactFileResolver artifactFileResolver;
    private final Log log;
    private final File workingDirectory;
    private final File javaHome;

    public JavaPluginBuilder(ArtifactFileResolver artifactFileResolver,Log log,  File workingDirectory)
    {
        this.artifactFileResolver = artifactFileResolver;
        this.log = log;
        this.workingDirectory = workingDirectory;

        this.javaHome = getJavaHome();
    }

    public File getPlugin(OutputTarget outputTarget) {
        Artifact artifact = artifactFileResolver.createDependencyArtifact(outputTarget.getPluginArtifact());
        if(artifact ==null){
            throw new InitializationException("Could not resolve artifact for " + outputTarget.getPluginArtifact());
        }

        List<File> libs = artifactFileResolver.resolveArtifacts(artifact);

        if(Os.isFamily(Os.FAMILY_WINDOWS)) {
            return buildWindowsPlugin(outputTarget,libs);
        } else {
            return buildUnixPlugin(outputTarget,libs);
        }

    }

    private File buildUnixPlugin(OutputTarget outputTarget,List<File> libs) {

        File javaExecutable = new File(javaHome, "bin/java");

        File shellScript = new File(workingDirectory,outputTarget.getName());
        if(shellScript.exists()){
            log.info("Unix plugin already exists: " + shellScript);
            return shellScript;
        }

        try(PrintWriter out = new PrintWriter(shellScript)) {
            out.println("#!/bin/sh");
            out.println();
            out.print("CP=");
            out.println(libs.stream().map(File::getAbsolutePath).collect(Collectors.joining(":","\"","\"")));
            out.print("\"");
            out.print(javaExecutable.getAbsolutePath());
            out.print("\" -cp $CP ");
            out.println(outputTarget.getMainClass());
            out.println();
        } catch (IOException e) {
            throw new InitializationException("Error creating plugin script for " + outputTarget.getName(),e);
        }

        shellScript.setExecutable(true);
        return shellScript;
    }

    private File buildWindowsPlugin(OutputTarget outputTarget,List<File> libs) {

        File javaExecutable = new File(javaHome, "bin/java.exe");

        File shellScript = new File(workingDirectory,outputTarget.getName()+".cmd");
        if(shellScript.exists()){
            log.info("Unix plugin already exists: " + shellScript);
            return shellScript;
        }

        try(PrintWriter out = new PrintWriter(shellScript)) {
            out.println("@echo off");
            out.print("set CP=");
            out.println(libs.stream().map(File::getAbsolutePath).collect(Collectors.joining(";","\"","\"")));
            out.print("\"");
            out.print(javaExecutable.getAbsolutePath());
            out.print("\" -cp %CP% ");
            out.println(outputTarget.getMainClass());
            out.println();
        } catch (IOException e) {
            throw new InitializationException("Error creating plugin script for " + outputTarget.getName(),e);
        }

        //shellScript.setExecutable(true);
        return shellScript;
    }

    private File getJavaHome() {
        String javaHome = System.getProperty("java.home");
        if(javaHome == null) {
            throw new InitializationException("Java home could not be detected.");
        }

        log.debug("Detected java home: " + javaHome);
        return new File(javaHome);
    }
}
