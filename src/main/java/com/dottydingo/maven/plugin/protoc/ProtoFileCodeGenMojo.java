package com.dottydingo.maven.plugin.protoc;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Resource;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Mojo(name = "generate-from-protos",
        defaultPhase = LifecyclePhase.GENERATE_SOURCES,
        requiresDependencyResolution = ResolutionScope.COMPILE)
public class ProtoFileCodeGenMojo extends AbstractCodegenMojo {
    private static final String DEFAULT_INCLUDE = "**/*.proto*";

    /**
     * A list of &lt;include&gt; elements specifying the protobuf definition files (by pattern)
     * that should be included in compilation.
     * When not specified, the default includes will be:
     * <code>
     * &lt;includes&gt;
     * &nbsp;&lt;include&gt;**&#47;*.proto&lt;/include&gt;
     * &lt;/includes&gt;
     * </code>
     */
    @Parameter(
            required = false
    )
    protected String[] includes = {DEFAULT_INCLUDE};

    /**
     * A list of &lt;exclude&gt; elements specifying the protobuf definition files (by pattern)
     * that should be excluded from compilation.
     * When not specified, the default excludes will be empty:
     * <code>
     * &lt;excludes&gt;
     * &lt;/excludes&gt;
     * </code>
     */
    @Parameter(
            required = false
    )
    protected String[] excludes = {};

    /**
     * A flag indicating if proto files in the project dependencies should be used when compiling.
     * Defaults to true.
     */
    @Parameter(required = false, defaultValue = "true")
    protected boolean useDependencyProtos = true;

    /**
     * The base directory containing the .proto files to generate code for.
     */
    @Parameter(required = true)
    private File protoFileDirectory;

    @Parameter(
            required = true,
            readonly = true,
            defaultValue = "${project.build.directory}/copied-protos"
    )
    private File protoCopyDirectory;

    @Override
    protected void addBuilderParameters(Protoc.Builder builder) {
        builder.addProtoPath(protoFileDirectory);
        if(useDependencyProtos) {
            builder.addProtoPath(protoCopyDirectory);
        }
    }

    @Override
    protected List<String> getProtoFiles() {
        if (!protoFileDirectory.exists() || protoFileDirectory.isFile()) {
            throw new InitializationException("'inputFileDescriptorSet' must be an existing directory.");
        }

        try {
            String include = String.join(",", includes.length == 0 ? new String[]{DEFAULT_INCLUDE} : includes);
            String exclude = String.join(",", excludes);
            return FileUtils.getFileNames(protoFileDirectory, include, exclude, true);
        } catch (IOException e) {
            throw new InitializationException("Error reading proto files from directory " + protoFileDirectory.getAbsolutePath(), e);
        }
    }

    @Override
    protected void attachFiles() {
        Resource resource = new Resource();
        resource.setDirectory(protoFileDirectory.getAbsolutePath());
        resource.setIncludes(Arrays.asList(includes));
        resource.setExcludes(Arrays.asList(excludes));
        resource.setTargetPath(protoResourceLocation);
        project.addResource(resource);
    }

    @Override
    protected void processAdditionalDependencies(ArtifactFileResolver artifactFileResolver) {
        if (!useDependencyProtos) {
            getLog().debug("Skipping dependency processing.");
            return;
        }

        getLog().info("Searching for proto files in dependencies");

        Set<Artifact> artifacts = new HashSet<>(project.getCompileArtifacts());

        if (artifacts.isEmpty()) {
            getLog().info("No dependencies found");
            return;
        }

        try {
            FileUtils.forceMkdir(protoCopyDirectory);
        } catch (IOException e) {
            throw new InitializationException("Error proto copy directory " + protoCopyDirectory.getAbsolutePath());
        }

        List<File> dependencyFiles = artifacts.stream().map(Artifact::getFile).collect(Collectors.toList());
        dependencyFiles.forEach(f -> {
            getLog().info("File: " + f.toString());
            copyProtos(protoCopyDirectory,f);
        });

    }

    private void copyProtos(File root, File dependencyJar) {
        try {
            ZipFile zipFile = new ZipFile(dependencyJar);
            List<ZipEntry> entries = zipFile.stream().
                    filter(e->e.getName().endsWith(".proto")).collect(Collectors.toList());

            entries.forEach(e-> {
                copyEntry(root, zipFile, e);
            });
        } catch (IOException e) {
            throw new ExecutionException("Error processing dependency " + dependencyJar.toString());
        }
    }

    private void copyEntry(File root, ZipFile zipFile, ZipEntry zipentry) {
        getLog().info("  Copying " + zipentry.getName());
        try {
            Path dest = new File(root, zipentry.getName()).toPath();
            Files.createDirectories(dest.getParent());
            Files.copy(zipFile.getInputStream(zipentry), dest, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioException) {
            throw new ExecutionException("Error copying file", ioException);
        }
    }
}
