package com.dottydingo.maven.plugin.protoc;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolutionRequest;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.artifact.resolver.ResolutionErrorHandler;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.repository.RepositorySystem;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.Os;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singleton;

public class ArtifactFileResolver {
    private final Log log;
    private final ArtifactRepository localRepository;
    private final List<ArtifactRepository> remoteRepositories;
    private final MavenSession session;
    private final RepositorySystem repositorySystem;
    private final ResolutionErrorHandler resolutionErrorHandler;
    private final File workingDirectory;
    private final Artifact projectArtifact;


    public ArtifactFileResolver(Log log,
                                ArtifactRepository localRepository,
                                List<ArtifactRepository> remoteRepositories,
                                MavenSession session,
                                RepositorySystem repositorySystem,
                                ResolutionErrorHandler resolutionErrorHandler,
                                File workingDirectory,
                                Artifact projectArtifact) {
        this.log = log;
        this.localRepository = localRepository;
        this.remoteRepositories = remoteRepositories;
        this.session = session;
        this.repositorySystem = repositorySystem;
        this.resolutionErrorHandler = resolutionErrorHandler;
        this.workingDirectory = workingDirectory;
        this.projectArtifact = projectArtifact;
    }

    public Set<Artifact> getArtifacts(Artifact artifact) {
        return getArtifacts(singleton(artifact));
    }

    public Set<Artifact> getArtifacts(Set<Artifact> artifactSet) {
        ArtifactResolutionRequest request = new ArtifactResolutionRequest()
                .setArtifact(projectArtifact)
                .setResolveRoot(false)
                .setResolveTransitively(false)
                .setArtifactDependencies(artifactSet)
                .setManagedVersionMap(emptyMap())
                .setLocalRepository(localRepository)
                .setRemoteRepositories(remoteRepositories)
                .setOffline(session.isOffline())
                .setForceUpdate(session.getRequest().isUpdateSnapshots())
                .setServers(session.getRequest().getServers())
                .setMirrors(session.getRequest().getMirrors())
                .setProxies(session.getRequest().getProxies());

        ArtifactResolutionResult result = repositorySystem.resolve(request);

        try {
            resolutionErrorHandler.throwErrors(request, result);
        } catch (final ArtifactResolutionException e) {
            throw new InitializationException("Unable to resolve artifact: " + e.getMessage(), e);
        }

        final Set<Artifact> artifacts = result.getArtifacts();

        if (artifacts == null || artifacts.isEmpty()) {
            throw new InitializationException("Unable to resolve artifact");
        }
        return artifacts;
    }

    public File resolveBinaryArtifact(Artifact artifact) {

        Set<Artifact> artifacts = getArtifacts(artifact);

        final Artifact resolvedBinaryArtifact = artifacts.iterator().next();
        if (log.isDebugEnabled()) {
            log.debug("Resolved artifact: " + resolvedBinaryArtifact);
        }

        String targetFileName = artifact.getFile().getName();
        if (Os.isFamily(Os.FAMILY_WINDOWS) && !targetFileName.endsWith("exe")) {
            targetFileName += ".exe";
        }

        File file = copyFile(artifact, targetFileName);
        if (!Os.isFamily(Os.FAMILY_WINDOWS)) {
            file.setExecutable(true);
        }
        return file;
    }


    public List<File> resolveArtifacts( Artifact artifact) {
        return getArtifacts(artifact).stream()
                .map(a -> copyFile(a, a.getFile().getName()))
                .collect(Collectors.toList());
    }

    public Artifact createDependencyArtifact(String artifactSpec){
        final String[] parts = artifactSpec.split(":");
        if (parts.length < 3 || parts.length > 5) {
            throw new ConfigurationException(
                    "Invalid artifact specification format"
                            + ", expected: groupId:artifactId:version[:type[:classifier]]"
                            + ", actual: " + artifactSpec);
        }

        Dependency dependency = new Dependency();
        dependency.setGroupId(parts[0]);
        dependency.setArtifactId(parts[1]);
        dependency.setVersion(parts[2]);
        dependency.setType(parts.length >= 4 ? parts[3] : null);
        dependency.setClassifier(parts.length == 5 ? parts[4] : null);
        dependency.setScope(Artifact.SCOPE_RUNTIME);
        return createDependencyArtifact(dependency);
    }

    public Artifact createDependencyArtifact(Dependency dependency) {
        return repositorySystem.createDependencyArtifact(dependency);
    }


    private File copyFile(Artifact artifact, String targetFileName) {
        File targetFile = new File(workingDirectory, targetFileName);
        if (targetFile.exists()) {
            log.info("Binary " + targetFile.getAbsolutePath() + "exists.");
            return targetFile;
        }

        try {
            FileUtils.copyFile(artifact.getFile(), targetFile);
        } catch (IOException e) {
            throw new InitializationException("Error copying artifact binary to " + targetFile, e);
        }

        return targetFile;
    }

}
