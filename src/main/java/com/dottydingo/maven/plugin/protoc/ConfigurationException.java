package com.dottydingo.maven.plugin.protoc;

public class ConfigurationException extends RuntimeException
{
    public ConfigurationException(String message)
    {
        super(message);
    }
}
