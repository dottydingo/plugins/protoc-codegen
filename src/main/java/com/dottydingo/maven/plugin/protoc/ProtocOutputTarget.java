package com.dottydingo.maven.plugin.protoc;

public class ProtocOutputTarget
{
    private final String type;
    private final String plugin;
    private final String location;

    public ProtocOutputTarget(String type, String plugin, String location)
    {
        this.type = type;
        this.plugin = plugin;
        this.location = location;
    }

    public String getType()
    {
        return type;
    }

    public String getPlugin()
    {
        return plugin;
    }

    public String getLocation()
    {
        return location;
    }
}
