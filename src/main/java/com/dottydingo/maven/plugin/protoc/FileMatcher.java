package com.dottydingo.maven.plugin.protoc;


import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FileMatcher
{
    private final List<Pattern> includes;
    private final List<Pattern> excludes;

    public FileMatcher(String[] includes, String[] excludes)
    {
        this.includes = buildPatterns(includes);
        this.excludes = buildPatterns(excludes);
    }

    public boolean match(String value) {
        return includes.stream().anyMatch(pattern -> pattern.matcher(value).find()) &&
                excludes.stream().noneMatch(pattern -> pattern.matcher(value).find());
    }

    private List<Pattern> buildPatterns(String[] values) {
        return Arrays.stream(values).map(Pattern::compile).collect(Collectors.toList());
    }


}
