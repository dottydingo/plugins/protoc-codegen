package com.dottydingo.maven.plugin.protoc;

import org.apache.maven.plugins.annotations.Parameter;

public class OutputTarget
{
    /**
     * The name of the output target. This should correspond to the name used for the protoc output.
     */
    @Parameter(
            required = true
    )
    private String name;

    /**
     * The plugin artifact specification, in {@code groupId:artifactId:version[:type[:classifier]]} format.
     * When this parameter is set, the plugin attempts to resolve the specified artifact as {@code protoc} executable.
     */
    @Parameter(
            required = false
    )
    private String pluginArtifact;

    /**
     * The type of plugin if pluginArtifact is specified. Values are NATIVE, JAVA
     */
    @Parameter(
            required = true,
            defaultValue = "SYSTEM"
    )
    private PluginType pluginType = PluginType.SYSTEM;

    /**
     * The main class for a {@code JAVA} @Code PluginType}
     */
    @Parameter(
            required = false
    )
    private String mainClass;

    public String getName()
    {
        return name;
    }

    public String getPluginArtifact()
    {
        return pluginArtifact;
    }

    public PluginType getPluginType()
    {
        return pluginType;
    }

    public String getMainClass() {
        return mainClass;
    }

    @Override
    public String toString() {
        return "OutputTarget{" +
                "name='" + name + '\'' +
                ", pluginArtifact='" + pluginArtifact + '\'' +
                ", pluginType=" + pluginType +
                ", mainClass='" + mainClass + '\'' +
                '}';
    }
}
