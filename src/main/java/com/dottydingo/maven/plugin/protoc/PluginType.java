package com.dottydingo.maven.plugin.protoc;

public enum PluginType {
    SYSTEM,
    NATIVE,
    JAVA
}
