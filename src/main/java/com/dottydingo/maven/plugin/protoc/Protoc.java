package com.dottydingo.maven.plugin.protoc;

import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Protoc
{
    private final String executable;
    private final List<ProtocOutputTarget> outputTargets;
    private final String inputFileDescriptorSet;
    private final List<String> protoFiles;
    private final boolean useArgumentFile;
    private final File workingDirectory;
    private final List<File> protoPaths;
    private final CommandLineUtils.StringStreamConsumer error;
    private final CommandLineUtils.StringStreamConsumer output;

    private Protoc(String executable,
                   List<ProtocOutputTarget> outputTargets,
                   String inputFileDescriptorSet,
                   List<String> protoFiles,
                   boolean useArgumentFile,
                   File workingDirectory,
                   List<File> protoPaths) {
        this.executable = executable;
        this.outputTargets = outputTargets;
        this.inputFileDescriptorSet = inputFileDescriptorSet;
        this.protoFiles = protoFiles;
        this.useArgumentFile = useArgumentFile;
        this.workingDirectory = workingDirectory;
        this.protoPaths = protoPaths;
        this.error = new CommandLineUtils.StringStreamConsumer();
        this.output = new CommandLineUtils.StringStreamConsumer();
    }

    public int execute(Log log) throws CommandLineException {
        Commandline commandline = new Commandline();
        commandline.setExecutable(executable);
        List<String> args = createArguments();

        if(log.isDebugEnabled()) {
            log.debug("protoc arguments:");
            args.forEach(arg->log.debug("  " + arg));
        }

        if (useArgumentFile) {
            try {
                File argFile = createArgumentFile(args);
                log.debug("Using argument file: " + argFile);
                commandline.addArguments(new String[]{"@"+argFile.getAbsolutePath()});
            } catch (IOException e) {
                throw new CommandLineException("Error creating argument file.",e);
            }
        }
        else {
            commandline.addArguments(args.toArray(args.toArray(new String[0])));
        }

        int attemptsLeft = 3;
        while (true) {
            try {
                return CommandLineUtils.executeCommandLine(commandline,null,output,error);
            } catch (CommandLineException e) {
                if(--attemptsLeft == 0 || e.getCause() == null) {
                    throw e;
                }
                log.warn("Unable to run protoc, will retry " + attemptsLeft + " times.",e);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ignore){}
            }
        }
    }

    public List<String> createArguments() {
        List<String> arguments = new ArrayList<>();

        if(inputFileDescriptorSet != null) {
            arguments.add("--descriptor_set_in="+inputFileDescriptorSet);
        }

        protoPaths.forEach(p->arguments.add("--proto_path="+ p));

        outputTargets.forEach(t->{
            if (t.getPlugin() != null) {
                arguments.add("--plugin=protoc-gen-"+t.getType()+"="+t.getPlugin());
            }
            arguments.add("--"+t.getType()+"_out="+t.getLocation());
        });

        arguments.addAll(protoFiles);

        return arguments;
    }

    public String getOutput() {
        return fixUnicodeOutput(output.getOutput());
    }

    public String getError() {
        return fixUnicodeOutput(error.getOutput());
    }

    private String fixUnicodeOutput(final String message) {
        return new String(message.getBytes(), StandardCharsets.UTF_8);
    }

    private File createArgumentFile(List<String> arguments) throws IOException
    {
        File argfile = File.createTempFile("protoc",null,workingDirectory);
        argfile.deleteOnExit();
        try (PrintWriter printWriter = new PrintWriter(argfile)) {
            arguments.forEach(printWriter::println);
            printWriter.flush();
            return argfile;
        }
    }

    public static final class Builder {
        private final String executable;
        private final List<ProtocOutputTarget> outputTargets;
        private final List<String> protoFiles;
        private String inputFileDescriptorSet;
        private boolean useArgumentFile;
        private File workingDirectory;
        private List<File> protoPaths;

        public Builder(String executable) {
            if (executable == null) {
                throw new ConfigurationException("Executable can not be null");
            }
            this.executable = executable;
            this.outputTargets = new ArrayList<>();
            this.protoFiles = new ArrayList<>();
            this.protoPaths = new ArrayList<>();
        }

        public Builder addOutputTarget(ProtocOutputTarget outputTarget) {
            if (outputTarget == null) {
                throw new ConfigurationException("Output target can not be null.");
            }
            outputTargets.add(outputTarget);
            return this;
        }

        public Builder addOutputTargets(List<ProtocOutputTarget> outputTargets) {
            if (outputTargets == null) {
                throw new ConfigurationException("Output targets can not be null.");
            }
            this.outputTargets.addAll(outputTargets);
            return this;
        }

        public Builder addProtoFile(String protoFile) {
            protoFiles.add(protoFile);
            return this;
        }

        public Builder addProtoFiles(List<String> protoFiles) {
            this.protoFiles.addAll(protoFiles);
            return this;
        }

        public Builder setInputFileDescriptorSet(String inputFileDescriptorSet) {
            this.inputFileDescriptorSet = inputFileDescriptorSet;
            return this;
        }

        public Builder useArgumentFile(boolean useArgumentFile) {
            this.useArgumentFile = useArgumentFile;
            return this;
        }

        public Builder setWorkingDirectory(File workingDirectory) {
            this.workingDirectory = workingDirectory;
            return this;
        }

        public Builder addProtoPath(File protoPath) {
            this.protoPaths.add(protoPath);
            return this;
        }

        public Protoc build() {

            if (workingDirectory == null) {
                throw new ConfigurationException("'workingDirectory' can not be null.");
            }

            if (protoFiles.isEmpty()) {
                throw new ConfigurationException("No proto files set.");
            }

            if(outputTargets.isEmpty()) {
                throw new ConfigurationException("No output targets specified.");
            }

            return new Protoc(executable,
                    outputTargets,
                    inputFileDescriptorSet,
                    protoFiles,
                    useArgumentFile,
                    workingDirectory,
                    protoPaths);
        }
    }
}
