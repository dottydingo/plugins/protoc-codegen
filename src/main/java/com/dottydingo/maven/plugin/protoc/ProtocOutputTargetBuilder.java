package com.dottydingo.maven.plugin.protoc;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.artifact.Artifact;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ProtocOutputTargetBuilder
{
    private final ArtifactFileResolver artifactFileResolver;
    private final JavaPluginBuilder javaPluginBuilder;
    private final File outputDirectory;

    public ProtocOutputTargetBuilder(ArtifactFileResolver artifactFileResolver,
                                     JavaPluginBuilder javaPluginBuilder,
                                     File outputDirectory)
    {
        this.artifactFileResolver = artifactFileResolver;
        this.javaPluginBuilder = javaPluginBuilder;
        this.outputDirectory = outputDirectory;
    }

    public List<ProtocOutputTarget> buildTargets(List<OutputTarget> targets)
    {
        List<ProtocOutputTarget> results = new ArrayList<>();

        for (OutputTarget target : targets) {
            System.out.println(target);
            switch (target.getPluginType()) {
                case SYSTEM:
                    validateSystem(target);
                    results.add(new ProtocOutputTarget(target.getName(), null, outputDirectory.getAbsolutePath()));
                    break;
                case NATIVE:
                    validateNative(target);
                    results.add(new ProtocOutputTarget(target.getName(),getNativePlugin(target.getPluginArtifact()),outputDirectory.getAbsolutePath()));
                    break;
                case JAVA:
                    validateJava(target);
                    results.add(new ProtocOutputTarget(target.getName(),javaPluginBuilder.getPlugin(target).getAbsolutePath(),outputDirectory.getAbsolutePath()));
                    break;
                default:
                    throw new InitializationException("Invalid plugin type:"+ target.getPluginType());
            }
        }

        return results;
    }

    private String getNativePlugin(String pluginArtifact) {
        Artifact artifact = artifactFileResolver.createDependencyArtifact(pluginArtifact);
        if(artifact == null) {
            throw new InitializationException(String.format("Could not create and artifact for '%s'",pluginArtifact));
        }
        return artifactFileResolver.resolveBinaryArtifact(artifact).getAbsolutePath();
    }

    static void validateSystem(OutputTarget outputTarget) {
        assertRequired(outputTarget.getName(),"Name is required [" + outputTarget +"]");
    }

    static void validateNative(OutputTarget outputTarget) {
        assertRequired(outputTarget.getName(),"name is required [" + outputTarget +"]");
        assertRequired(outputTarget.getPluginArtifact(),"pluginArtifact is required [" + outputTarget +"]");
    }

    static void validateJava(OutputTarget outputTarget) {
        assertRequired(outputTarget.getName(),"name is required [" + outputTarget +"]");
        assertRequired(outputTarget.getPluginArtifact(),"pluginArtifact is required [" + outputTarget +"]");
        assertRequired(outputTarget.getMainClass(),"mainClass is required [" + outputTarget +"]");
    }

    static void assertRequired(String value, String errorMessage) {
        if(StringUtils.isBlank(value)) {
            throwError(errorMessage);
        }
    }

    static void throwError(String message) {
        throw new InitializationException(message);
    }

}
