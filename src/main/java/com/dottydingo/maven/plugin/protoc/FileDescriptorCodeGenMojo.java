package com.dottydingo.maven.plugin.protoc;


import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Resource;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Mojo(name = "generate-from-descriptor",
        defaultPhase = LifecyclePhase.GENERATE_SOURCES,
        requiresDependencyResolution = ResolutionScope.COMPILE)
public class FileDescriptorCodeGenMojo extends AbstractCodegenMojo
{
    private static final String DEFAULT_INCLUDE = ".*";

    /**
     * A regex indicating what protos should be included from the file descriptor set
     * When not specified, the default include will be:
     * <code>
     * &lt;include&gt;.*&lt;/include&gt;
     * </code>
     */
    @Parameter(
            required = false
    )
    protected String include = DEFAULT_INCLUDE;

    /**
     * A regex indicating what protos should be excluded from the file descriptor set
     * that should be excluded from compilation.
     * When not specified, the default exclude will be empty.
     */
    @Parameter(
            required = false
    )
    protected String exclude = "";

    /**
     * The location of the file descriptor set to generate code for.
     */
    @Parameter(required = true)
    private File inputFileDescriptorSet;

    @Override
    protected void addBuilderParameters(Protoc.Builder builder)
    {
        builder.setInputFileDescriptorSet(inputFileDescriptorSet.getAbsolutePath());
    }

    @Override
    protected List<String> getProtoFiles()
    {
        if (!inputFileDescriptorSet.exists() || !inputFileDescriptorSet.isFile()) {
            throw new InitializationException("'inputFileDescriptorSet' must be an existing file.");
        }

        byte[] content;
        try (FileInputStream fis = new FileInputStream(inputFileDescriptorSet)) {
            content = IOUtils.toByteArray(fis);
        } catch (IOException e) {
            throw new InitializationException("Error reading inputFileDescriptorSet " + inputFileDescriptorSet.getAbsolutePath());
        }

        FileMatcher fileMatcher = new FileMatcher(new String[]{include} ,
                StringUtils.isEmpty(exclude) ? new String[0] : new String[]{exclude});

        try {
            DescriptorProtos.FileDescriptorSet descriptorSet = DescriptorProtos.FileDescriptorSet.newBuilder()
                    .mergeFrom(content).build();

            return descriptorSet.getFileList().stream()
                    .map(DescriptorProtos.FileDescriptorProto::getName)
                    .filter(fileMatcher::match)
                    .collect(Collectors.toList());
        } catch (InvalidProtocolBufferException e) {
            throw new InitializationException("Invalid file descriptor set",e);
        }
    }

    @Override
    protected void attachFiles()
    {
        Resource resource = new Resource();
        resource.setDirectory(inputFileDescriptorSet.getParent());
        resource.setIncludes(List.of(inputFileDescriptorSet.getName()));
        resource.setTargetPath(protoResourceLocation);
        project.addResource(resource);
        getLog().info("Attaching resource: " + resource);
    }
}
