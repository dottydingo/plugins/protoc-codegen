outputDirectory = new File(basedir, 'target/generated-sources/protoc-gen');
assert outputDirectory.exists();
assert outputDirectory.isDirectory();

generatedJavaFile = new File(outputDirectory, 'com/dottydingo/test/InvoiceServiceGrpc.java');
assert generatedJavaFile.exists();
assert generatedJavaFile.isFile();

content = generatedJavaFile.text;
assert content.contains('package com.dottydingo.test');
assert content.contains('class InvoiceServiceImplBase');

generatedJavaFile = new File(outputDirectory, 'com/dottydingo/test/InvoiceServiceOuterClass.java');
assert generatedJavaFile.exists();
assert generatedJavaFile.isFile();

content = generatedJavaFile.text;
assert content.contains('package com.dottydingo.test');
assert content.contains('class InvoiceService');
assert content.contains('class Invoice');
assert content.contains('class InvoiceRequest');
assert content.contains('class InvoiceResponse');

return true;