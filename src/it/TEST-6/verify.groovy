outputDirectory = new File(basedir, 'target/generated-sources/protoc-gen');
assert outputDirectory.exists();
assert outputDirectory.isDirectory();

generatedJavaFile = new File(outputDirectory, 'com/dottydingo/test/Invoices.java');
assert generatedJavaFile.exists();
assert generatedJavaFile.isFile();

content = generatedJavaFile.text;
assert content.contains('package com.dottydingo.test');
assert content.contains('class Invoices');
assert content.contains('class Invoice');

generatedJavaFile = new File(outputDirectory, 'com/dottydingo/test/Contacts.java');
assert !generatedJavaFile.exists();

return true;