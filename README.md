# Protoc Code Generator Maven Plugin

This is Maven plugin that will download and run protoc to generate code for either
a `FileDescriptorSet` or `.proto` files. Native and Java based protoc plugins are 
supported as long as they can be resolved as Maven artifacts. This plugin was inspired by
the [protoc-jar-maven-plugin](https://github.com/os72/protoc-jar-maven-plugin) and the
[protobuf-maven-plugin](https://github.com/xolstice/protobuf-maven-plugin).


## Usage

This plugin supports the following goals:

| Goal | Phase | Description |
| --- |---|--- |
| generate-from-descriptor | generate-sources | Run protoc sourcing `FileDescriptors` in the specified `FileDescriptorSet` |
| generate-from-protos | generate-sources | Run protoc against `.proto` files specified in a source directory |

<hr>

###  generate-from-descriptor

**Parameters**

| Name | Type | Required | Default | Description |
| --- | --- | --- | --- | --- |
| protocArtifact | String | Yes | N/A |  Protobuf compiler artifact specification, in `groupId:artifactId:version[:type[:classifier]]` format. |
| inputFileDescriptorSet | File | Yes | The location of the `FileDescriptorSet` to use to generate code from. |
| outputTargets | [OutputTarget[]](#outputtarget) | Yes | N/A | The output targets. |
| outputDirectory | File | Yes | `${project.build.directory}/generated-sources/protoc-gen` | The location to store the generated source files. |
| workingDirectory | File | No | `${project.build.directory}/protoc-plugins` | A directory where temporary files will be generated. |
| attachProtoSources | boolean | No | `true` | Whether the `FileDescriptorSet` should be attached as a resource to the build. |
| protoResourceLocation | String | No | `proto` | The resource location for the `FileDescriptorSet` if `attachProtoSources` is `true` |
| useArgumentFile | boolean | No | `false` | If an argument file should be created for protoc. This should only be necessary if the command like for protoc gets too long. |
| include | String | No | `.*` | A regex for selecting which `FileDescriptors` in the `FileDescriptorSet` should be included for code generation. |
| exclude | String | No | N/A | A regex for selecting which `FileDescriptors` in the `FileDescriptorSet` should NOT be included for code generation. |

###  generate-from-protos

**Parameters**

| Name | Type | Required | Default | Description |
| --- | --- | --- | --- | --- |
| protocArtifact | String | Yes | N/A |  Protobuf compiler artifact specification, in `groupId:artifactId:version[:type[:classifier]]` format. |
| protoFileDirectory | File | Yes | The base directory of the `.proto` files to generate code for.  |
| outputTargets | [OutputTarget[]](#outputtarget) | Yes | N/A | The output targets. |
| outputDirectory | File | Yes | `${project.build.directory}/generated-sources/protoc-gen` | The location to store the generated source files. |
| workingDirectory | File | No | `${project.build.directory}/protoc-plugins` | A directory where temporary files will be generated. |
| attachProtoSources | boolean | No | `true` | Whether the `FileDescriptorSet` should be attached as a resource to the build. |
| protoResourceLocation | String | No | `proto` | The resource location for the `FileDescriptorSet` if `attachProtoSources` is `true` |
| useArgumentFile | boolean | No | `false` | If an argument file should be created for protoc. This should only be necessary if the command like for protoc gets too long. |
| useDependencyProtos | boolean | No | `true` | If `*.proto` files should be imported from project dependencies. Note: this is also required when using the well known protobuf types. |
| includes | String[] | No | `"**/*.proto*` | An ant path expression for selecting the `.proto` files to be included for code generation. |
| excludes | String[] | No | N/A | An ant path expression for selecting the `.proto` files to be included for code generation. |

### OutputTarget

| Name | Type | Required | Default | Description |
| --- | --- | --- | --- | --- |
| name | String | Yes | N/A | The name of the output target. This should correspond to the name used for the protoc output. |
| pluginType | Enum [`SYSTEM`,`NATIVE`,`JAVA`] | Yes | `SYSTEM` | The type of output target that this is. <br> `SYSTEM` is anything supported in the protoc binary. <br> `NATIVE` is a native executable plugin. <br> `Java` is a Java based plugin. |
| pluginArtifact | String | For `NATIVE` and `JAVA` types | N/A | The plugin artifact specification, in `groupId:artifactId:version[:type[:classifier]]` format. |
| mainClass | String | For `JAVA` types | N/A | The name of the main class for a Java based plugin |

## Examples

These examples assume that the OS platform is being detected using the `os-maven-plugin` extension: 

```xml
<extensions>
  <extension>
    <groupId>kr.motd.maven</groupId>
    <artifactId>os-maven-plugin</artifactId>
    <version>1.6.1</version>
  </extension>
</extensions>
```

### generate-from-descriptor

Run protoc against the specified `FileDescriptorSet` with the `java` output target. 

```xml
<plugin>
    <groupId>com.dottydingo.plugins</groupId>
    <artifactId>protoc-codegen</artifactId>
    <version>${protoc-codegen.version}</version>
    <configuration>
        <protocArtifact>com.google.protobuf:protoc:${protobuf.version}:exe:${os.detected.classifier}</protocArtifact>
        <inputFileDescriptorSet>src/main/proto/test-descriptor.bin</inputFileDescriptorSet>
        <outputTargets>
            <target>
                <name>java</name>
            </target>
        </outputTargets>
    </configuration>
    <executions>
        <execution>
            <goals>
                <goal>generate-from-descriptor</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

Run protoc against the specified `FileDescriptorSet` with the `java` output target and
the java-grpc native plugin

```xml
<plugin>
    <groupId>com.dottydingo.plugins</groupId>
    <artifactId>protoc-codegen</artifactId>
    <version>${protoc-codegen.version}</version>
    <configuration>
        <protocArtifact>com.google.protobuf:protoc:${protobuf.version}:exe:${os.detected.classifier}</protocArtifact>
        <inputFileDescriptorSet>src/main/proto/test-descriptor.bin</inputFileDescriptorSet>
        <outputTargets>
            <target>
                <name>java</name>
            </target>
            <target>
                <name>grpc</name>
                <pluginArtifact>io.grpc:protoc-gen-grpc-java:${grpc.version}:exe:${os.detected.classifier}</pluginArtifact>
                <pluginType>NATIVE</pluginType>
            </target>
        </outputTargets>
    </configuration>
    <executions>
        <execution>
            <goals>
                <goal>generate-from-descriptor</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

Run protoc against the specified `FileDescriptorSet` with the `java` output target, 
the java-grpc native plugin, and the Vertx Grpc Java based plugin.

```xml
<plugin>
    <groupId>com.dottydingo.plugins</groupId>
    <artifactId>protoc-codegen</artifactId>
    <version>${protoc-codegen.version}</version>
    <configuration>
        <protocArtifact>com.google.protobuf:protoc:${protobuf.version}:exe:${os.detected.classifier}</protocArtifact>
        <inputFileDescriptorSet>src/main/proto/test-descriptor.bin</inputFileDescriptorSet>
        <outputTargets>
            <target>
                <name>java</name>
            </target>
            <target>
                <name>grpc</name>
                <pluginArtifact>io.grpc:protoc-gen-grpc-java:${grpc.version}:exe:${os.detected.classifier}</pluginArtifact>
                <pluginType>NATIVE</pluginType>
            </target>
            <target>
                <name>vertx-grpc</name>
                <pluginArtifact>io.vertx:vertx-grpc-protoc-plugin:4.0.0.Beta2:jar</pluginArtifact>
                <pluginType>JAVA</pluginType>
                <mainClass>io.vertx.grpc.protoc.plugin.VertxGrpcGenerator</mainClass>
            </target>
        </outputTargets>
    </configuration>
    <executions>
        <execution>
            <goals>
                <goal>generate-from-descriptor</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

### generate-from-protos

Run protoc against the specified the `.proto` files located in the specified `protoFileDirectory`.

```xml
<plugin>
    <groupId>com.dottydingo.plugins</groupId>
    <artifactId>protoc-codegen</artifactId>
    <version>${protoc-codegen.version}</version>
    <configuration>
        <protocArtifact>com.google.protobuf:protoc:${protobuf.version}:exe:${os.detected.classifier}</protocArtifact>
        <protoFileDirectory>src/main/proto</protoFileDirectory>
        <outputTargets>
            <target>
                <name>java</name>
            </target>
        </outputTargets>
    </configuration>
    <executions>
        <execution>
            <goals>
                <goal>generate-from-protos</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```
